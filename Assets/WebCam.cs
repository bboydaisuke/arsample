﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WebCam : MonoBehaviour {
	WebCamTexture webcamTexture;
    public RawImage _image;
	
	void Start () {

        int camHeight_px = Screen.height;
        int camWidth_px = Screen.width;

		var devices = WebCamTexture.devices;
		
		if (devices.Length > 0)
		{
			//var euler = transform.localRotation.eulerAngles;
			//webcamTexture = new WebCamTexture(camWidth_px,camHeight_px, 10);
            webcamTexture = new WebCamTexture();
			
			//iPhone,Androidの場合はカメラの向きを縦にする
            /*
			if(Application.platform == RuntimePlatform.IPhonePlayer||Application.platform == RuntimePlatform.Android){
				transform.localRotation = Quaternion.Euler( euler.x, euler.y, euler.z - 90 );
			}
             */

            _image.texture = webcamTexture;
            _image.material.mainTexture = webcamTexture;
			webcamTexture.Play();
			
		}else
		{
			Debug.Log("Webカメラが検出できませんでした");
			return;
		}
	}
	
	//Sceneを変更する場合にカメラを止める
	public void EndCam(){
		webcamTexture.Stop ();
	}
}